package com.ocado.uok.skilo.scanner;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Rozszerzenie MutableLiveData aby zachowywał sie jak zdarzenia.
 * Patrz https://github.com/googlesamples/android-architecture-components/issues/63
 */
public class SingleLiveEvent<T> extends MutableLiveData<T> {

    private final AtomicBoolean pending = new AtomicBoolean(false);

    public void observe(LifecycleOwner owner, Observer<T> observer) {
        // Observe the internal MutableLiveData
        super.observe(owner, t -> {
            if (pending.compareAndSet(true, false)) {
                observer.onChanged(t);
            }
        });
    }

    @Override
    public void setValue(T t) {
        pending.set(true);
        super.setValue(t);
    }

    public void call() {
        setValue(null);
    }

}