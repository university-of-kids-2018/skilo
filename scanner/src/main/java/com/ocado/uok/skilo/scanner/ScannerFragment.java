package com.ocado.uok.skilo.scanner;

import android.Manifest;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModelProviders;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class ScannerFragment extends Fragment implements ZXingScannerView.ResultHandler {
    private static final int REQUEST_CAMERA_PERMISSION = 201;

    /**
     * Komponent View z biblioteki zeewnetrznej, który wyświetla podgląd kamery
     *  oraz obserwuje kamerę i wyszukue QR kody.
     *
     * Jeśli znajdzie QR kody, odczytuje go, następnie wstrzymuje podgląd i powiadamia sluchaczy
     *  o nowo odczytanym kodzie.
     *
     * Sluchacz po wykonaniu stosownych akcji powinien wznowić podgląd kamery.
     */
    private ZXingScannerView scannerView;

    private MutableLiveData<String> lastScanned;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        scannerView = new ZXingScannerView(getActivity());
        scannerView.setResultHandler(this);

        ScannerResult scannerResult = ViewModelProviders.of(getActivity()).get(ScannerResult.class);
        scannerResult.getResumeCamera().observe(this, (Void) -> scannerView.resumeCameraPreview(this));
        lastScanned = scannerResult.getLastScanned();

        return scannerView;
    }

    /**
     * Do włączenia kamery potrzebne są uprawnienia. Należy sprawdzić czy zostały wcześniej nadane.
     * Jeśli nie to należy poprosić o uprawnienia i oczekiwać na odpowiedź (patrz metoda #onRequestPermissionsResult)
     */
    private void startCamera() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(
                    new String[] { Manifest.permission.CAMERA },
                    REQUEST_CAMERA_PERMISSION);
        }
        else {
            scannerView.startCamera();
        }
    }

    /**
     * Reagowanie na nadanie uprawnień do otworzenia kamery - jeśli uprawenia nadane, to otwórz kamerę
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CAMERA_PERMISSION) {
            startCamera();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    @Override
    public void onResume() {
        super.onResume();
        scannerView.setLaserEnabled(false);
        scannerView.setResultHandler(this);
        startCamera();
    }

    @Override
    public void handleResult(Result rawResult) {
        try {
            String scannedText = rawResult.getText().toUpperCase();
            lastScanned.postValue(scannedText);
        } catch (IllegalArgumentException e) {
            scannerView.resumeCameraPreview(this);
            // ignore invalid product code and resume
        }
    }
}
