package com.ocado.uok.skilo.scanner;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

/**
 * ViewModel do komunikacji z ScannerFragment.
 * Po zeskanowaniu komponent ustawia obiekt lastScanned i wstrzymuje podgląd kamery.
 * Aby znowić podglad, nalezy wywołać resumeCamera.call()
 */
public class ScannerResult extends ViewModel {

    /**
     * Ostatnio zeskanowany napis. Obserwowanie umożliwia wykrycie zdarzenia zeskanowania.
     */
    private final MutableLiveData<String> lastScanned = new MutableLiveData<>();

    /**
     * Zawołanie na tym obiekcie spowoduje wznowienie podglądu kamery.
     */
    private final SingleLiveEvent<Void> resumeCamera = new SingleLiveEvent<>();

    public MutableLiveData<String> getLastScanned() {
        return lastScanned;
    }

    public SingleLiveEvent<Void> getResumeCamera() {
        return resumeCamera;
    }
}
