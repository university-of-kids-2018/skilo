package com.ocado.skilo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.ocado.skilo.basket.TimeRecord;
import com.ocado.skilo.result.ResultService;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static android.view.Gravity.RIGHT;

/**
 * Wyświetla listę wyników
 */
public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        setContentView(R.layout.activity_result);
        TableLayout scoreLayout = findViewById(R.id.resultTable);
        ResultService resultService = new ResultService(this);
        List<TimeRecord> timeRecords = resultService.getAll();
        for(int i = 0; i < timeRecords.size(); i++) {
            scoreLayout.addView(
                    createScoreEntry(i, new Date(timeRecords.get(i).getStartTimestamp()),
                    timeRecords.get(i).getReadableDuration(),
                    displayMetrics.widthPixels
            ));
        }
    }

    private TableRow createScoreEntry(int position, Date time, String result, int width) {
        TableRow l = createScoreRow();
        l.addView(createScorePart(position + ".", width / 8));
        SimpleDateFormat dateFormat = new SimpleDateFormat(getString(R.string.dateTimeFormat));
        l.addView(createScorePart(dateFormat.format(time), width / 3));
        l.addView(createScorePart(result, width / 4));
        return l;
    }

    private TextView createScorePart(String value, int width) {
        TextView tv = new TextView(this);
        tv.setMinWidth(width);
        tv.setGravity(RIGHT);
        tv.setText(value);
        tv.setBackgroundResource(R.drawable.row_border);
        return tv;
    }

    private TableRow createScoreRow() {
        return new TableRow(this);
    }
}
