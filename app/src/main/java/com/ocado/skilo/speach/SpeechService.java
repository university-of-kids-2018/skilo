package com.ocado.skilo.speach;

import android.content.Context;
import android.speech.tts.TextToSpeech;

import static android.speech.tts.TextToSpeech.QUEUE_FLUSH;
import static java.util.Locale.UK;

public class SpeechService {

    private TextToSpeech textToSpeech;

    public void speak(String s) {
        textToSpeech.speak(s, QUEUE_FLUSH, null, null);
    }

    public void init(Context context) {
        this.textToSpeech = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    textToSpeech.setLanguage(UK);
                }
            }
        });
    }

    public void destroy() {
        textToSpeech.shutdown();
        textToSpeech = null;
    }

//    private static final int REQ_CODE_SPEECH_INPUT = 1001;
//
//    public void promptSpeechInput(Activity activity) {
//        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
//        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
//        try {
//            activity.startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
//        } catch (ActivityNotFoundException a) {
//            Toast.makeText(activity.getApplicationContext(),
//                    activity.getString(R.string.speech_not_supported),
//                    Toast.LENGTH_SHORT).show();
//        }
//    }
//
//    public Command processResult(int requestCode, int resultCode, Intent data) {
//        switch (requestCode) {
//            case REQ_CODE_SPEECH_INPUT: {
//                if (resultCode == RESULT_OK && null != data) {
//                    ArrayList<String> result = data
//                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
//                    for (String s : result) {
//                        Log.i("SpeechService", "######## " + s);
//                    }
//                    return Command.toEnum(result.get(0));
//                }
//            }
//        }
//        return UNKNOW;
//    }
}
