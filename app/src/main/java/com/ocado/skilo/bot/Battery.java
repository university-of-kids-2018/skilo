package com.ocado.skilo.bot;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.concurrent.atomic.AtomicInteger;

public class Battery extends ViewModel {

    private static final String TAG = Battery.class.getName();

    private static final int ONE_SECOND = 1000;
    private static final int INITIAL_POWER = 100; // percent
    private static final int POWER_CONSUMPTION = 2;

    private Handler handler = new Handler(Looper.getMainLooper());

    private final MutableLiveData<AtomicInteger> power = new MutableLiveData<>();

    public Battery() {
        power.setValue(new AtomicInteger(INITIAL_POWER));
        usePower();
    }

    public void rechargeBattery() {
        this.power.postValue(new AtomicInteger(INITIAL_POWER));
        Log.i(TAG, "Batteries fully charged");
    }

    private void usePower() {
        handler.postDelayed(() -> {
            AtomicInteger currentPower = power.getValue();
            AtomicInteger usedPower = new AtomicInteger(POWER_CONSUMPTION);
            AtomicInteger newPower = positiveSubtract(currentPower, usedPower);

            power.postValue(newPower);
            if (newPower.get() > 0) {
                usePower();
            }
        }, ONE_SECOND);
    }

    private AtomicInteger positiveSubtract(AtomicInteger a, AtomicInteger b) {
        return new AtomicInteger(Math.max(0, a.get() - b.get()));
    }

    public MutableLiveData<AtomicInteger> getPower() {
        return power;
    }
}
