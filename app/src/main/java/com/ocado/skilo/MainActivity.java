package com.ocado.skilo;

import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.ocado.skilo.audio.SfxPlayer;
import com.ocado.skilo.collision.CollisionDetectionService;
import com.ocado.skilo.collision.CollisionListener;

/**
 * Główny ekran z instrukcją i możliwością wyboru rozpoczęcia skanowania lub przejścia do wyników
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button startBtn;
    private Button scoreBtn;
    private SfxPlayer sfxPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initViews();
        sfxPlayer = new SfxPlayer();
    }

    private void initViews() {
        startBtn = findViewById(R.id.startBtn);
        startBtn.setOnClickListener(this);

        scoreBtn = findViewById(R.id.scoreBtn);
        scoreBtn.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        sfxPlayer.init(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        sfxPlayer.destroy();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.startBtn:
                sfxPlayer.playOnce(R.raw.testsound);
                startActivity(new Intent(MainActivity.this, ScanActivity.class));
                break;
            case R.id.scoreBtn:
                startActivity(new Intent(MainActivity.this, ResultActivity.class));
                break;
        }
    }
}
