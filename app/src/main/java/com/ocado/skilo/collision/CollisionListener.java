package com.ocado.skilo.collision;

public interface CollisionListener {
    void collisionDetected();
}
