package com.ocado.skilo.collision.dto;

public class PhoneShiftVector extends PhonePosition {
    public static PhoneShiftVector fromPositions(PhonePosition from, PhonePosition to) {
        return new PhoneShiftVector(to.getX() - from.getX(), to.getY() - from.getY(), to.getZ() - from.getZ());
    }

    private PhoneShiftVector(float x, float y, float z) {
        super(x, y, z);
    }

    public PhoneShiftVector minus(PhoneShiftVector vector) {
        return new PhoneShiftVector(getX() - vector.getX(), getY() - vector.getY(), getZ() - vector.getZ());
    }

    public float getManhattanDistance() {
        return Math.abs(getX()) + Math.abs(getY()) + Math.abs(getZ());
    }
}
