package com.ocado.skilo.collision;

import android.util.Log;

import com.ocado.skilo.collision.dto.PhonePosition;
import com.ocado.skilo.collision.dto.PhonePositionMeasurement;
import com.ocado.skilo.collision.dto.PhoneShiftVector;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

class OldCollisionDetector {

    private static final int MONITOR_DURATION_NANOS = 1000_000_000;
    private static final float EPSILON = 1f; // TODO experiment with the value

    private List<PhonePositionMeasurement> measurements = new ArrayList<>();
    private Set<CollisionListener> listeners = new HashSet<>();
    private Long lastDetectionTimestamp = null;

    public void handlePositionMeasurement(long timestamp, float x, float y, float z) {
        System.out.println(String.format("Acc: %s, %s, %s", x, y, z));

        if (lastDetectionTimestamp == null ||
                hasEnoughTimeElapsed(lastDetectionTimestamp, timestamp)) {

            lastDetectionTimestamp = null;
            addMeasurement(timestamp, x, y, z);
            clearOldMeasurements(timestamp);

            if (isCollisionDetected()) {
                System.out.println("######## BANG! ############");

                lastDetectionTimestamp = timestamp;
                notifyListeners();
            }
        }
    }

    public void registerListener(CollisionListener listener) {
        this.listeners.add(listener);
    }

    public void unregisterListener(CollisionListener listener) {
        this.listeners.remove(listener);
    }

    private void addMeasurement(long timestamp, float x, float y, float z) {
        PhonePositionMeasurement measurement = new PhonePositionMeasurement(timestamp, new PhonePosition(x, y, z));
        this.measurements.add(measurement);
    }

    private void clearOldMeasurements(long lastTimestamp) {
        ListIterator<PhonePositionMeasurement> iterator = this.measurements.listIterator();
        while(iterator.hasNext()) {
            if(hasEnoughTimeElapsed(iterator.next().getTimestamp(), lastTimestamp)) {
                iterator.remove();
            }
        }
    }

    private boolean hasEnoughTimeElapsed(long fromTimestamp, long toTimestamp) {
        return toTimestamp - fromTimestamp > MONITOR_DURATION_NANOS;
    }

    private boolean isCollisionDetected() {
        int measurementCount = measurements.size();
        if(measurementCount < 3) {
            return false;
        }
        int middleIndex = measurementCount / 2;
        PhonePosition first = measurements.get(0).getPosition();
        PhonePosition middle = measurements.get(middleIndex).getPosition();
        PhonePosition last = measurements.get(measurementCount - 1).getPosition();
        float distance = PhoneShiftVector.fromPositions(first, middle).minus(PhoneShiftVector.fromPositions(middle, last)).getManhattanDistance();
        if(distance > 0)  {
            Log.i("CollisionDetector", String.format("Detected collision with distance found: %s", distance)); // Temporary in order to experiment with epsilon
        }
        return distance > EPSILON;
    }

    private void notifyListeners() {
        for(CollisionListener listener: this.listeners) {
            listener.collisionDetected();
        }
    }
}
