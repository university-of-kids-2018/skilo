package com.ocado.skilo.collision;

import java.util.HashSet;
import java.util.Set;

class CollisionDetector {

    private static final int MIN_TIME_BEFORE_NEXT_DETECTION = 1000_000_000;
    private static final double THRESHOLD = 5d; // TODO experiment with the value

    private Set<CollisionListener> listeners = new HashSet<>();
    private Long lastDetectionTimestamp = null;

    public void handlePositionMeasurement(long timestamp, float x, float y, float z) {

        if (lastDetectionTimestamp == null ||
                hasEnoughTimeElapsed(lastDetectionTimestamp, timestamp)) {

            lastDetectionTimestamp = null;

            if (isCollisionDetected(x, y, z)) {
                lastDetectionTimestamp = timestamp;
                notifyListeners();
            }
        }
    }

    public void registerListener(CollisionListener listener) {
        this.listeners.add(listener);
    }

    public void unregisterListener(CollisionListener listener) {
        this.listeners.remove(listener);
    }

    private boolean hasEnoughTimeElapsed(long fromTimestamp, long toTimestamp) {
        return toTimestamp - fromTimestamp > MIN_TIME_BEFORE_NEXT_DETECTION;
    }

    private boolean isCollisionDetected(double x, double y, double z) {
        final double acc = Math.sqrt(x*x + y*y + z*z);
        System.out.println(acc);
        return acc > THRESHOLD;
    }

    private void notifyListeners() {
        for(CollisionListener listener: this.listeners) {
            listener.collisionDetected();
        }
    }
}


