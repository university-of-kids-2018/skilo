package com.ocado.skilo.collision.dto;

public class PhonePositionMeasurement {
    private long timestamp;
    private PhonePosition position;

    public PhonePositionMeasurement(long timestamp, PhonePosition position) {
        this.timestamp = timestamp;
        this.position = position;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public PhonePosition getPosition() {
        return position;
    }
}
