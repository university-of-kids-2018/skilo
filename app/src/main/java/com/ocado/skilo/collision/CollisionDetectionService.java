package com.ocado.skilo.collision;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

/**
 * Nasluchuje informacje o przyspieszeniu komórki w 3 płaszczyznach.
 * Przekazuje informacje do detektora.
 * Pozwala na podłączenie sluchaczy zdarzenia wykrycia kolizji.
 */
public class CollisionDetectionService {

    private final SensorManager sensorManager;

    // czujnik połóżenia telefonu
    private final Sensor senAccelerometer;

    private final CollisionDetector collisionDetector = new CollisionDetector();

    // słuchacz czujnika połózenia telefonu
    private final SensorEventListener listener = new SensorEventListener() {

        @Override
        public void onSensorChanged(SensorEvent event) {
            if(event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];
                collisionDetector.handlePositionMeasurement(event.timestamp, x, y, z);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    public CollisionDetectionService(Context context) {
        sensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        if (senAccelerometer == null) {
            Log.e(getClass().getName(), "Cannot obtain the requested sensor: TYPE_LINEAR_ACCELERATION");
        }
    }

    public void start() {
        if (senAccelerometer != null) {
            sensorManager.registerListener(listener, senAccelerometer, SensorManager.SENSOR_DELAY_UI);
        }
    }

    public void stop() {
        sensorManager.unregisterListener(listener);
    }

    public void registerListener(CollisionListener listener) {
        collisionDetector.registerListener(listener);
    }

    public void unregisterListener(CollisionListener listener) {
        collisionDetector.unregisterListener(listener);
    }

}
