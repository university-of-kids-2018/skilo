package com.ocado.skilo.collision.dto;

public class PhonePosition {
    private float x;
    private float y;
    private float z;

    public PhonePosition(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }
}
