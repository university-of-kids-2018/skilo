package com.ocado.skilo.result;

import android.content.Context;
import android.util.Log;

import com.ocado.skilo.basket.TimeRecord;

import org.apache.commons.io.IOUtils;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Obsługuje zapis/odczyt informacji o ukończonych zadaniach do/z pliku
 */
public class ResultService {
    private static final String TAG = ResultService.class.getName();
    private static final String RESULT_FILE_NAME = "results.json";
    private static final String ENCODING = "UTF-8";

    private final Context appContext;
    private final Serializer serializer = new Serializer();

    public ResultService(Context appContext) {
        this.appContext = appContext;
    }

    public void save(TimeRecord timeRecord) {
        FileOutputStream stream = null;
        try {
            List<TimeRecord> timeRecords = getAll();
            timeRecords.add(timeRecord);
            stream = appContext.openFileOutput(RESULT_FILE_NAME, Context.MODE_PRIVATE);
            IOUtils.write(serializer.serialize(timeRecords), stream, ENCODING);
        } catch (IOException e) {
            Log.w(TAG, "Failed to store shopping tasks", e);
        } finally {
            IOUtils.closeQuietly(stream);
        }
    }

    public List<TimeRecord> getAll() {
        FileInputStream stream = null;
        try {
            stream = appContext.getApplicationContext().openFileInput(RESULT_FILE_NAME);
            return serializer.deserialize(IOUtils.toString(stream, ENCODING));
        } catch (IOException e) {
            Log.w(TAG, "Failed to read shopping tasks", e);
        } finally {
            if(stream != null) {
                IOUtils.closeQuietly(stream);
            }
        }
        return new ArrayList<>();
    }
}
