package com.ocado.skilo.result;

import com.ocado.skilo.basket.TimeRecord;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;
import com.squareup.moshi.Types;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

class Serializer {

    public String serialize(List<TimeRecord> timeRecords) {
        return getAdapter().toJson(timeRecords);
    }

    public List<TimeRecord> deserialize(String content) {
        try {
            return getAdapter().fromJson(content);
        } catch (IOException e) {
            return new ArrayList<>();
        }
    }

    private JsonAdapter<List<TimeRecord>> getAdapter() {
        Moshi moshi = new Moshi.Builder().build();
        Type type = Types.newParameterizedType(List.class, TimeRecord.class);
        return moshi.adapter(type);
    }

}
