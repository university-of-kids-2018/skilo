package com.ocado.skilo.audio;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.SoundPool;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.util.SparseIntArray;

import com.ocado.skilo.R;

public class SfxPlayer {
    private static final String TAG = "SFX-Player";

    private static final int PRIORITY_ONE = 1;
    private static final int NO_LOOP = 0;
    private static final float FULL_VOLUME = 1.0f;
    private static final float NORMAL_SPEED = 1.0f;

    private SoundPool preemptingSoundPool;
    /**
     * mapuje identyfikator dźwięku (np. 'R.raw.testsound') na identyfikator w puli 'preemptingSoundPool'
     */
    private SparseIntArray soundsLoaded;
    /**
     * czy ifentyfikator dźwięku (np. 'R.raw.testsound') jest załadowany
     */
    private SparseBooleanArray soundsReady;

    public SfxPlayer() {
        soundsLoaded = new SparseIntArray();
        soundsReady = new SparseBooleanArray();
    }

    public int playOnce(int soundId) {
        Log.i(TAG, String.format("Playback of sound id: %d requested", soundId));
        if (isReadyToPlay(soundId)) {
            return preemptingSoundPool.play(soundsLoaded.get(soundId), FULL_VOLUME, FULL_VOLUME, PRIORITY_ONE, NO_LOOP, NORMAL_SPEED);
        }
        Log.w(TAG, String.format("Sound id: %d not ready to play", soundId));
        return -1;
    }

    public void init(Context context) {
        Log.d(TAG, "Initializing SFX Player...");
        preemptingSoundPool = buildPreemptiveSoundPool(context);
        Log.d(TAG, "SFX Player initialized");
    }

    public void destroy() {
        Log.d(TAG, "Destroying SFX Player...");
        preemptingSoundPool.release();
        preemptingSoundPool = null;
        soundsLoaded.clear();
        soundsReady.clear();
        Log.d(TAG, "SFX Player destroyed");
    }

    private SoundPool buildPreemptiveSoundPool(Context context) {
        SoundPool soundPool = new SoundPool.Builder()
                .setMaxStreams(1)
                .setAudioAttributes(
                        new AudioAttributes.Builder()
                                .setUsage(AudioAttributes.USAGE_GAME)
                                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                                .build()
                ).build();

        setupLoadCompleteListener(soundPool);
        loadAllSfx(context, soundPool);

        return soundPool;
    }

    private void setupLoadCompleteListener(SoundPool soundPool) {
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                soundsReady.put(sampleId, Boolean.TRUE);
            }
        });
    }

    private void loadAllSfx(Context context, SoundPool soundPool) {
        loadSfx(context, soundPool, R.raw.testsound);
    }

    private void loadSfx(Context context, SoundPool soundPool, int soundId) {
        int streamId = soundPool.load(context, soundId, PRIORITY_ONE);
        soundsLoaded.put(soundId, streamId);
    }

    private boolean isReadyToPlay(int soundId) {
        return isSoundLoaded(soundId) && isSoundReady(soundId);
    }

    private boolean isSoundReady(int soundId) {
        return soundsReady.get(soundsLoaded.get(soundId));
    }

    private boolean isSoundLoaded(int soundId) {
        return soundsLoaded.get(soundId) != 0;
    }
}
