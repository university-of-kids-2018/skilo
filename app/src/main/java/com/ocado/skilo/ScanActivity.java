package com.ocado.skilo;

import android.app.AlertDialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.ocado.skilo.basket.Product;
import com.ocado.skilo.basket.ShoppingTask;
import com.ocado.skilo.basket.TimeRecord;
import com.ocado.skilo.bot.Battery;
import com.ocado.skilo.collision.CollisionDetectionService;
import com.ocado.skilo.result.ResultService;
import com.ocado.skilo.speach.SpeechService;
import com.ocado.uok.skilo.scanner.ScannerResult;
import com.ocado.uok.skilo.scanner.SingleLiveEvent;

/**
 * Ekran ze skanowaniem produktów.
 */
public class ScanActivity extends AppCompatActivity {
    private static final String CLASS_NAME = ScanActivity.class.getName();

    private SpeechService speechService;
    private CollisionDetectionService collisionDetectionService;
    private ShoppingTask shoppingTask;
    private Battery battery;
    private SingleLiveEvent<Void> resumeCamera;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        speechService = new SpeechService();

        collisionDetectionService = new CollisionDetectionService(this);
        collisionDetectionService.registerListener(() ->
                RingtoneManager.getRingtone(
                        ScanActivity.this, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
                ).play());

        shoppingTask = ViewModelProviders.of(this).get(ShoppingTask.class);
        shoppingTask.getFinishedTimeRecord().observe(this, t -> {
            if (t != null) finishTask(t);
        });

        battery = ViewModelProviders.of(this).get(Battery.class);
        battery.getPower().observe(this, f -> {
            if (f.get() == 0) {
                startActivity(new Intent(ScanActivity.this, EmptyBatteryActivity.class));
                finish();
            }
        });

        ScannerResult scannerResult = ViewModelProviders.of(this).get(ScannerResult.class);
        resumeCamera = scannerResult.getResumeCamera();
        scannerResult.getLastScanned().observe(this, scannedText -> onScannedText(scannedText));
    }

    private void onScannedText(String scannedText) {
        if ("POWER".equals(scannedText)) {
            onBatteryRecharged();
        } else {
            final Product p = Product.valueOf(scannedText);
            onProductScanned(p);
        }
    }

    private void addToBasket(Product product) {
        speak(product);
        shoppingTask.addToBasket(product);
    }

    public void onBatteryRecharged() {
        battery.rechargeBattery();
        restoreCamera();
    }

    public void onProductScanned(Product product) {
        Log.i(CLASS_NAME, String.format("Scanned product: %s", product));
        speechService.speak("Scanned product " + product.name());

        if (shoppingTask.canBeAddedToBasket(product)) {
            new AlertDialog.Builder(this)
                    .setTitle("Zeskanowano " + product.name())
                    .setMessage("Czy dodać produkt do koszyka?")
                    .setPositiveButton("TAK", (dialog, id) -> addToBasket(product))
                    .setNegativeButton("NIE", (dialog, id) -> {
                    })
                    .setCancelable(true)
                    .setOnDismissListener(dialog -> restoreCamera())
                    .create()
                    .show();
        } else if (! shoppingTask.isComplete()) {
            Log.i(CLASS_NAME, String.format("Cannot add %s to basket", product));
            speechService.speak(product + " cannot be added to basket");
            restoreCamera();
        }
    }

    private void speak(Product product) {
        speechService.speak("Product " + product + " added to basket");
    }

    private void finishTask(TimeRecord timeRecord) {
        Log.i(CLASS_NAME, String.format("Completed in %s millis", timeRecord.getDuration()));
        Log.i(CLASS_NAME, "Basket is complete!");

        new ResultService(this).save(timeRecord);
        new AlertDialog.Builder(this)
                .setTitle("Wszystkie produkty w koszyku")
                .setMessage("Zamówienie zostało skompletowane w czasie " + timeRecord.getReadableDuration())
                .setNeutralButton("KONIEC", (dialog, id) -> finish())
                .setCancelable(false)
                .create()
                .show();
    }

    private void restoreCamera() {
        // czekaj pół sekundy przed wzowieniem skanera
        new Handler().postDelayed(() -> resumeCamera.call(), 500);
    }

    @Override
    protected void onStart() {
        super.onStart();
        speechService.init(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        speechService.destroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        collisionDetectionService.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        collisionDetectionService.start();
    }
}
