package com.ocado.skilo.basket;

import java.util.ArrayList;
import java.util.List;

public class Basket {

    private List<Product> products = new ArrayList<>();

    /**
     * Zlicz ilość danego produktu
     */
    public int count(Product searched) {
        int count = 0;
        for (Product p: products) {
            if (p == searched) {
                count++;
            }
        }
        return count;
    }

    /**
     * Dodaj produkt do koszyka
     */
    public void add(Product product) {
        products.add(product);
    }

    /**
     * Czy ten koszyk zawiera wszystkie elementy z drugiego koszyka
     */
    public boolean containsAll(Basket anotherBasket) {
        ArrayList productsCopy = new ArrayList(products);
        for (Product p: anotherBasket.products) {
            boolean result = productsCopy.remove(p);
            if (!result) {
                return false;
            }
        }
        return true;
    }

    /**
     * Zwraca liste wszystkich produktow
     */
    public List<Product> getProducts() {
        return products;
    }
}
