package com.ocado.skilo.basket;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

/**
 * Model dla widoku z zadaniem skanowania.
 * Zawiera informacje co należy zebrać oraz co jest już zebrane.
 *
 * Obsługuje całą logikę związaną z koszykiem:
 *  - operacje dodawania do koszyka
 *  - czy można dodać dany produkt itp.
 *  - powiadamia widoki o zmianach (np. że koszyk jest już zebrany)
 */
public class ShoppingTask extends ViewModel {

    /**
     * Lista zebranych produktów w koszyku.
     * Opakowana w MutableLiveData, dzięki czemu można obserwować jej zmiany.
     */
    private final MutableLiveData<Basket> collectedEntries = new MutableLiveData<>();

    // lista produktów, które należy zebrać
    private Basket shoppingEntries = ShoppingListGenerator.buildRandomShoppingList(5);

    // mierzenie czasu
    private final RunningTimer timer = Timer.start();

    /**
     * Pomiar zakończonego zadania
     * Dzięki MutableLiveData można zaobserwować kiedy zadanie zostanie zakończone
     */
    private final MutableLiveData<TimeRecord> finishedTimeRecord = new MutableLiveData<>();

    public ShoppingTask() {
        collectedEntries.setValue(new Basket());
    }

    public void addToBasket(Product product) {
        Basket actualCollected = collectedEntries.getValue();
        actualCollected.add(product);

        collectedEntries.postValue(actualCollected);

        if (isComplete()) {
            finishTask();
        }
    }

    public boolean canBeAddedToBasket(Product product) {
        return collectedEntries.getValue().count(product) < shoppingEntries.count(product);
    }

    public boolean isComplete() {
        return collectedEntries.getValue().containsAll(shoppingEntries);
    }

    private void finishTask() {
        final TimeRecord timeRecord = this.timer.stop();
        this.finishedTimeRecord.postValue(timeRecord);
    }

    public Basket getShoppingEntries() {
        return shoppingEntries;
    }

    public MutableLiveData<Basket> getCollectedEntries() {
        return collectedEntries;
    }

    public MutableLiveData<TimeRecord> getFinishedTimeRecord() {
        return finishedTimeRecord;
    }
}
