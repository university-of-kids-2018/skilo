package com.ocado.skilo.basket;

import java.util.Date;

interface RunningTimer {
    TimeRecord stop();
}

/**
 * Pomiar czasu wykonywania zadania
 */
public class Timer {
    private Timer() {}

    public static RunningTimer start() {
        return new RunningTimer() {
            private final Long startTime = new Date().getTime();

            @Override
            public TimeRecord stop() {
                final Long endTime = new Date().getTime();
                return new TimeRecord(startTime, endTime);
            }
        };
    }
}

