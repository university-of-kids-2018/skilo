package com.ocado.skilo.basket;

import java.util.Random;

/**
 * Generuje losowy koszyk
 */
class ShoppingListGenerator {

    private static Random random = new Random();

    private ShoppingListGenerator() { }

    public static Basket buildRandomShoppingList(int size) {
        int productsCount = Product.values().length;

        Basket basket = new Basket();
        for (int i=0; i<size; i++) {
            int lot = random.nextInt(productsCount);
            basket.add(Product.values()[lot]);
        }
        return basket;
    }

}
