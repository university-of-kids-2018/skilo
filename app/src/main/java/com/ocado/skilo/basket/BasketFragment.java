package com.ocado.skilo.basket;

import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ocado.skilo.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Rysuje koszyk na ekranie ze skanowaniem oraz zaznacza, które produkty są już zebrane
 */
public class BasketFragment extends Fragment {

    private static final int HIGHLIGHT_COLOR = Color.argb(128, 153,255,102);

    // lista produktów które powinny być zebrane w koszyku
    private Basket basketToScan;

    // lista obrazków - wykorzystana będzie przy podświetlaniu zebranych produktów
    private Map<Product, List<ImageView>> imageMap;

    // obiekt UI na którym trzymane są obrazki z produktami
    private LinearLayout basketLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_basket, container, false);
        basketLayout = view.findViewById(R.id.basketLayout);
        return basketLayout;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // obserwuj stan koszyka, jeśli się zmieni to reaguj
        ShoppingTask shoppingTaskModel = ViewModelProviders.of(getActivity()).get(ShoppingTask.class);
        shoppingTaskModel.getCollectedEntries().observe(this, collected -> {
            highlightCollected(collected);
        });
        basketToScan = shoppingTaskModel.getShoppingEntries();

        initBasketImages();

    }

    // rysuje początkowy stan koszyka
    private void initBasketImages() {
        basketLayout.removeAllViews();
        imageMap = new HashMap<>();

        for (Product p: Product.values()) {
            int count = basketToScan.count(p);

            List<ImageView> images = new ArrayList<>();
            for (int i=0; i<count; i++) {
                ImageView image = new ImageView(requireActivity());
                image.setPadding(10, 10, 10, 10);
                image.setImageResource(p.getId());
                images.add(image);
                basketLayout.addView(image);
            }
            imageMap.put(p, images);
        }
    }

    // wyróżnij wszystkie zebrane produkty
    private void highlightCollected(Basket collected) {
        for (Product p: Product.values()) {
            int count = collected.count(p);
            highlightProduct(p, count);
        }
    }

    private void highlightProduct(Product product, int quantity) {
        List<ImageView> images = imageMap.get(product);
        if(images != null) {
            for (int i = 0; i < quantity && i < images.size(); i++) {
                images.get(i).setBackgroundColor(HIGHLIGHT_COLOR);
            }
        }
    }

}
