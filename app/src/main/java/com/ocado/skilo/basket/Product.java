package com.ocado.skilo.basket;

import com.ocado.skilo.R;

/**
 * Produkt
 */
public enum Product {

    APPLE(R.drawable.ic_apple),
    BANANA(R.drawable.ic_banana),
    ORANGE(R.drawable.ic_orange),
    ONION(R.drawable.ic_onion),
    CUCUMBER(R.drawable.ic_cucumber);

    private int id;

    Product(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
