package com.ocado.skilo.basket;

public class TimeRecord {

    private final long startTimestamp;
    private final long endTimestamp;

    public TimeRecord(long startTime, long endTime) {
        this.startTimestamp = startTime;
        this.endTimestamp = endTime;
    }

    public long getStartTimestamp() {
        return startTimestamp;
    }

    public long getEndTimestamp() {
        return endTimestamp;
    }

    public long getDuration() {
        return endTimestamp - startTimestamp;
    }

    public String getReadableDuration() {
        long totalSeconds = getDuration() / 1000;
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        long millis = getDuration() % 1000;
        return String.format("%02d:%02d.%03d", minutes, seconds, millis);
    }
}
